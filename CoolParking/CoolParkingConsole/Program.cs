﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.IO;
using CoolParkingConsole.Helpers;
using CoolParkingConsole.Functions;
namespace CoolParkingConsole
{
    class Program
    {
        static void ShowMenu(IParkingService parkingService){
            var menu = @$"Select one of following categories 1 - 9 or q to quit:{Environment.NewLine}
            1 - Current balance{Environment.NewLine}
            2 - Earned money{Environment.NewLine}
            3 - Available parking spots{Environment.NewLine}
            4 - Read current transactions{Environment.NewLine}
            5 - Read history of transactions{Environment.NewLine}
            6 - List current vehicles parked in parking{Environment.NewLine}
            7 - Park a vehicle{Environment.NewLine}
            8 - Checkout vehicle{Environment.NewLine}
            9 - Top up vehicle balance{Environment.NewLine}
            ";
            Console.WriteLine(menu);
            var choice = Console.ReadLine();
            while(choice!="q" && choice!="Q"){
                switch(choice){
                    case "1": {
                        Function.CurrentBalance(parkingService);
                        break;
                    }
                    case "2":{
                        Function.EarnedMoney(parkingService);
                        break;
                    }
                    case "3":{
                        Function.AvailableParkingPlaces(parkingService);
                        break;
                    }
                    case "4":{
                        Function.RecentTransactions(parkingService);
                        break;
                    }
                    case "5":{
                        Function.ReadLogHistory(parkingService);
                        break;
                    }
                    case "6":{
                        Function.CurrentParkedVehicles(parkingService);
                        break;
                    }
                    case "7":{
                        Function.AddToParking(parkingService);
                        break;
                    }
                    case "8":{
                        Function.RemoveFromParking(parkingService);
                        break;
                    }
                    case "9":{
                        Function.TopUpVehicle(parkingService, Helper.GetLicensePlate());
                        break;
                    }
                    case string q when (q.ToLower() == "q"):{
                        return;
                    }
                    default:{
                        Console.WriteLine("No valid case was selected");
                        break;
                    }
                }
                Console.WriteLine($"Type any key to continue");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine(menu);
                choice = Console.ReadLine();
            }
        }


        static void Main(string[] args)
        {
            ShowMenu(Helper.SetUp());
           File.Delete(Settings.LogFilePath);
        }
    }
}
