using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;


namespace CoolParkingConsole.Helpers
{
    public static class Helper
    {
        public static decimal? ValidateSumInput(string sum_input)
        {
            decimal sum;
            if (Decimal.TryParse(sum_input, out sum))
            {
                return sum;
            }
            else
            {
                Console.WriteLine("Entered input was invalid.");
                return null;
            }
        }
        public static bool YesOrNoQuestion()
        {
            Console.WriteLine("Input y - for yes or n - for no.");
            var answer = Console.ReadLine();
            if (answer.ToLower() == "y")
                return true;
            if (answer.ToLower() == "n")
                return false;
            throw new ArgumentException("No valid response was selected.");
        }
        public static VehicleType SelectVehicleType()
        {
            Console.WriteLine(@$"Select one of the following vehicle types 1 - 4:{Environment.NewLine}
            1 - Passenger car{Environment.NewLine}
            2 - Truck{Environment.NewLine}
            3 - Bus{Environment.NewLine}
            4 - Motorcycle{Environment.NewLine}");
            var choice = Console.ReadLine();
            switch (choice)
            {
                case "1": return VehicleType.PassengerCar;
                case "2": return VehicleType.Truck;
                case "3": return VehicleType.Bus;
                case "4": return VehicleType.Motorcycle;
                default: throw new ArgumentException("No valid type selected");
            }
        }
        public static string GetLicensePlate()
        {
            Console.WriteLine($"Please input valid license plate number of a vehicle");
            return Console.ReadLine();
        }
        public static IParkingService SetUp()
        {
            ITimerService withDrawTimer = new TimerService();
            withDrawTimer.Interval = Settings.ChargeRate;
            ITimerService logTimer = new TimerService();
            logTimer.Interval = Settings.LogRate;
            ILogService logService = new LogService(Settings.LogFilePath);
            IParkingService service = new ParkingService(withDrawTimer, logTimer, logService);
            logTimer.Start();
            withDrawTimer.Start();
            return service;
        }

    }

}