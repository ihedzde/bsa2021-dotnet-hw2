using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;
using CoolParkingConsole.Helpers;
namespace CoolParkingConsole.Functions
{
    public static class Function
    {
        public static void AddToParking(IParkingService service)
        {
            try
            {
                string plate;
                Console.WriteLine("Do you wish to generate new local licence plate?");
                if (Helper.YesOrNoQuestion())
                {
                    plate = Vehicle.GenerateRandomRegistrationPlateNumber();
                }
                else
                {
                    plate = Helper.GetLicensePlate();
                }
                VehicleType type = Helper.SelectVehicleType();
                decimal sum = 0;
                Console.WriteLine("Do you wish to top up?");
                if (Helper.YesOrNoQuestion())
                {
                    Console.WriteLine($"Please input top up sum: ");
                    var sum_input = Console.ReadLine();
                    decimal? output;
                    output = Helper.ValidateSumInput(sum_input);
                    if (output == null)
                        return;
                    sum = output ?? 0;
                }
                Vehicle vehicle = new Vehicle(plate, type, sum);
                service.AddVehicle(vehicle);
                Console.WriteLine($"{vehicle.ToString()} was added on the parking.");

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void RemoveFromParking(IParkingService service)
        {
            var plate = Helper.GetLicensePlate();
            try
            {
                var vehicle = service.GetVehicles().SingleOrDefault((v) => v.Id == plate);
                service.RemoveVehicle(plate);
                Console.WriteLine($"{vehicle} was removed.");
                if (vehicle.Balance > 0)
                    Console.WriteLine($"Get your change {vehicle.Balance}");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void CurrentParkedVehicles(IParkingService service)
        {
            var vehicles = service.GetVehicles();
            if (vehicles.Count == 0)
            {
                Console.WriteLine("No vehicles are on the parking.");
            }
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{vehicle} is parked");
            }
        }
        public static void ReadLogHistory(IParkingService service)
        {
            try
            {
                Console.WriteLine(service.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void RecentTransactions(IParkingService service)
        {
            var list = service.GetLastParkingTransactions();
            if (list.Length == 0)
            {
                Console.WriteLine("No recent transactions occured.");
                return;
            }
            foreach (var transactions in list)
            {
                Console.WriteLine(transactions.ToString());
            }
        }
        public static void EarnedMoney(IParkingService service)
        {
            decimal earned = 0;
            foreach (var transactions in service.GetLastParkingTransactions())
            {
                earned += transactions.Sum;
            }
            Console.WriteLine($"Earned {earned} amount");
        }
        public static void CurrentBalance(IParkingService service)
        {
            Console.WriteLine($"Current balance {service.GetBalance()}");
        }
        public static void AvailableParkingPlaces(IParkingService service)
        {
            Console.WriteLine($"There are {service.GetFreePlaces()} available places from {service.GetCapacity()} in the parking.");
        }
        public static void TopUpVehicle(IParkingService service, string licensePlate)
        {
            Console.WriteLine($"Please input top up sum: ");
            var sum_input = Console.ReadLine();
            try
            {
                decimal? output;
                output = Helper.ValidateSumInput(sum_input);
                if (output == null)
                    return;
                decimal sum = output ?? 0;
                service.TopUpVehicle(licensePlate, sum);
                Console.WriteLine($"The vehicle with license plate {licensePlate} was topped up.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}