﻿// TODO: [COOL-1] implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;
using Fare;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        #region Properties
        private readonly string id;
        private readonly VehicleType vehicleType;

        public string Id => id;
        public VehicleType VehicleType => vehicleType;
        public decimal Balance { get; internal set; }
        #endregion

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            #region Validation
            Regex rx = new Regex("(^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$)");
            if (balance < 0 || !rx.IsMatch(id))
            {
                string message = ((balance < 0) ? "Balance can't be negative. " : "") +
                                 ((!rx.IsMatch(id)) ? "License plate is invalid." : "");
                throw new ArgumentException(message);
            }
            #endregion
            this.id = id;
            this.vehicleType = vehicleType;

            Balance = balance;
        }
        #region Methods
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Xeger generator = new Xeger("([A-Z]{2}-[0-9]{4}-[A-Z]{2})");
            return generator.Generate();
        }
        public override string ToString()
        {
            string type = "";
            switch (VehicleType)
            {
                case VehicleType.Motorcycle: type = "motorcycle"; break;
                case VehicleType.Truck: type = "truck"; break;
                case VehicleType.Bus: type = "bus"; break;
                case VehicleType.PassengerCar: type = "passeger car"; break;
            }
            return $"Vehicle {Id} of type {type} has {Balance} on the balance";
        }
        #endregion
    }
}