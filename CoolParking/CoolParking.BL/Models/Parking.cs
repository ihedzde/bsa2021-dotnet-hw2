﻿// TODO: [COOL-3] implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace CoolParking.BL.Models
{

    public class Parking : IDisposable
    {
        private static Parking _instance;
        public decimal Balance { get; set; } = Settings.Balance;
        public ICollection<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public int GetCount => Vehicles.Count;
        public bool IsVehicalInParking(string vehicleId)
        {
            return Vehicles.SingleOrDefault<Vehicle>((v) => v.Id == vehicleId) != null;
        }
        private Parking()
        {

        }
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }
        #region Methods
        public Vehicle AddVehicle(Vehicle vehicle)
        {
            Vehicles.Add(vehicle);
            return vehicle;
        }
        public Vehicle GetVehicle(string vehicleId)
        {
            Vehicle vehicle = Vehicles.FirstOrDefault<Vehicle>((v) => v.Id == vehicleId);
            return vehicle;
        }
        public Vehicle RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = Vehicles.FirstOrDefault<Vehicle>((v) => v.Id == vehicleId);
            Vehicles.Remove(vehicle);
            return vehicle;
        }

        public void Dispose()
        {
            Vehicles.Clear();
            Parking._instance = null;
        }
        #endregion

    }

}