﻿// TODO: [COOL-6] implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services{
    public class LogService : ILogService
    {
        private readonly string _logFilePath;
        public LogService(string logFilePath){
            this._logFilePath = logFilePath;
        }
        public string LogPath => _logFilePath;

        public string Read()
        {
            if(!File.Exists(_logFilePath))
                throw new InvalidOperationException("File doesn't exist.");
            StringBuilder builder = new StringBuilder();
            using (var file = new StreamReader(_logFilePath))
            {
                string line;
               while ((line = file.ReadLine()) != null)
                {
                    builder.AppendLine(line);
                }
            }
            return builder.ToString();
        }

        public void Write(string logInfo)
        {
            if(logInfo.Length == 0)
                return;
            using (StreamWriter stream = new StreamWriter(_logFilePath, true))
            {
                stream.WriteLine(logInfo);
            }

        }
    }
}