﻿// TODO: [COOL-8] implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get => timer.Interval / 1000; set => timer.Interval = value * 1000; }
        private readonly Timer timer;

        public TimerService()
        {
            timer = new Timer();
            timer.Elapsed += (Object source, ElapsedEventArgs e) => { Elapsed.Invoke(source, e); };
        }
        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Enabled = true;
        }
        public void Stop()
        {
            timer.Enabled = false;
        }
    }
}