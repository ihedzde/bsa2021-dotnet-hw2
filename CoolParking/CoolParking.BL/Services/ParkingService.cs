﻿// TODO: [COOL-7] implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.Json;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;
        private readonly Parking parking;
        private readonly ICollection<TransactionInfo> history = new List<TransactionInfo>();
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.logTimer = logTimer;
            this.logService = logService;
            this.withdrawTimer = withdrawTimer;
            parking = Parking.GetInstance();
            InitTimers();
        }
        private void InitTimers()
        {
            withdrawTimer.Interval = Settings.ChargeRate;
            withdrawTimer.Elapsed += WithdrawOnEvent;
            logTimer.Interval = Settings.LogRate;
            logTimer.Elapsed += LogOnEvent;
            withdrawTimer.Start();
            logTimer.Start();
        }
        public void WithdrawOnEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in parking.Vehicles)
            {
                history.Add(ChargeVehicle(vehicle));
            }
        }
        public void LogOnEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var log in history)
            {
                stringBuilder.AppendLine(JsonSerializer.Serialize<TransactionInfo>(log));
            }
            logService.Write(stringBuilder.ToString()); //Weird requirment of WhenLogTimerIsElapsed_ThenWriteLogIsHappened unit test.
            history.Clear();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.IsVehicalInParking(vehicle.Id))
                throw new ArgumentException($"{vehicle.VehicleType} with {vehicle.Id} license plate already exists.");
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException("No free space left.");
            parking.AddVehicle(vehicle);
        }

        public void Dispose()
        {
            logTimer.Stop();
            withdrawTimer.Stop();
            logTimer.Dispose();
            parking.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingPlacesCount;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parking.GetCount;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return ((List<TransactionInfo>)history).ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>((IList<Vehicle>)parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (!parking.IsVehicalInParking(vehicleId))
                throw new ArgumentException($"Vechicle with {vehicleId} license plate isn't on parking.");
            var vehicle = parking.GetVehicle(vehicleId);
            if (GetFreePlaces() >= GetCapacity())
                throw new InvalidOperationException("No free space left, parking on full capacity.");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Invalid negative balance. Balance should be positive to remove a vehicle.");
            parking.RemoveVehicle(vehicleId);
        }
        private TransactionInfo ChargeVehicle(Vehicle vehicle)
        {
            var rate = Settings.GetRate(vehicle.VehicleType);
            var fineRate = Settings.FineRate;
            var previousBalance = vehicle.Balance;
            if (vehicle.Balance < 0)
                vehicle.Balance -= fineRate * rate;
            else if (vehicle.Balance < rate)
            {
                vehicle.Balance = (vehicle.Balance - rate) * fineRate;
            }
            else
                vehicle.Balance -= rate;
            var charge = previousBalance - vehicle.Balance;
            parking.Balance += charge;
            TransactionInfo info = new TransactionInfo
            {
                Sum = charge,
                VehicleId = vehicle.Id,
                Time = DateTime.Now
            };
            return info;
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Negative top up.");
            if (!parking.IsVehicalInParking(vehicleId))
                throw new ArgumentException($"Vechicle with {vehicleId} license plate isn't on parking.");
            var vehicle = parking.GetVehicle(vehicleId);
            vehicle.Balance += sum;
        }
    }
}